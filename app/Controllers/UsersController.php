<?php

namespace App\Controllers;

use App\Controllers\AppController;


class UsersController  extends AppController
{

    protected $model;
    protected $redirect;
    protected $root;

    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index($req, $res)
    {
        $this->View->render($res, 'index.twig');
    }

    public function insert_user($req, $res)
    {
        if ($form['password'] != "") {
            $options = [
                'salt' => random_bytes(22),
            ];
            $password = password_hash($form['password'], PASSWORD_BCRYPT, $options);
            $form['password'] = $password;
        } else {
            unset($form['password']);
        }
    }

    public function login($req, $res)
    {
        $username = $post['username'];
        $password = $post['password'];
        $user = $this->model->where('email', '=', $username)->get()->first();

        $password_db = $user->password;
        $password_verify = password_verify($password, $password_db);
    }
}
