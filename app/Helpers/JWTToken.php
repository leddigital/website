<?php

namespace App\Helpers;

use App\App;


class JWTToken extends App
{

    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke($req, $res, $next)
    {
        return $next($req, $res);
    }
}
