<?php
/*
 * ROTAS WEBSITE
 */
$app->get('[/]', 'App\Controllers\PagesController:index')->setName('index');
$app->get('/biblionat', 'App\Controllers\PagesController:biblionat')->setName('biblionat');
$app->get('/contato', 'App\Controllers\PagesController:contato')->setName('contato');
$app->get('/bioforcas', 'App\Controllers\PagesController:bioforcas')->setName('bioforcas');
$app->get('/bionews', 'App\Controllers\PagesController:bionews')->setName('bionews');
$app->get('/bionews/id', 'App\Controllers\PagesController:post')->setName('post');
$app->get('/eventos', 'App\Controllers\PagesController:eventos')->setName('eventos');
$app->get('/mip', 'App\Controllers\PagesController:mip')->setName('mip');
$app->get('/eventos/id', 'App\Controllers\PagesController:evento')->setName('evento');

/*
 * ROTAS DO ADMIN 
 */
$app->group("/admin", function () {
    $this->get("[/]", 'App\Controllers\Admin\PostsController:index')->setName('admin.news.index.dashboard');
})->add(new App\Helpers\CheckSessionCms($container));

/*
 * ROTAS DO API 
 */
$app->group("/api", function () {
    $this->post("/user/insert[/]", 'App\Controllers\UsersController:insert_user')->setName('api.users.insert');
})->add(new App\Helpers\JWTToken($container));
