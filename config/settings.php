<?php

return [
    'settings' => [
        'public_paths' => [
            '/admin/login' => 'GET',
            '/admin/logout' => 'GET',
            '/admin/user/login' => 'POST',
            '/admin/usuario/esqueci-minha-senha' => 'GET',
            '/admin/usuario/esqueci-minha-senha/enviar' => 'POST'
        ],
        'fullPathUrl' => false, //Aparece URL inteira nos arquivos estaticos
        'adminPath' => '/cms',
        'imgPath' => '/img/',
        'scriptPath' => '/js/',
        'cssPath' => '/css/',
        'pluginsPath' => '/plugins/',
        //Exibir erros
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
        //Informações complementares sobre o site
        'site_name' => "Policial Padrão",
        'cms_name' => 'Painel Administrativo',
        //Caminho dos arquivos de aplicação
        'app_path' => APP,
        //Caminho absoluto dos templates
        'template_path' => ROOT . DS . 'view',
        'determineRouteBeforeAppMiddleware' => true,
        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'policialpadrao',
            'username' => 'root',
            'password' => '15975320',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]
    ],
    'mail' => [
        'debug' => 2,
        'host' => ' smtp.gmail.com',
        'smtpAuth' => true,
        'username' => 'enzo.nagata@gmail.com',
        'password' => '78978010200389',
        'port' => 587
    ]
];
